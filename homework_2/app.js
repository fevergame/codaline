var express = require('express'),
    bodyParser = require('body-parser'),

    app = express();

    app.use( bodyParser.urlencoded({ extended: true }) );
    app.use( bodyParser.json() );

    app.get('/', function (req, res) {
      res.render('index.ejs');
    })

var data = {
    name: 'iam',
    surname: 'fevergame',
    email: 'fever@smile',
    age: 22,
            };

    app.get('/me', function (req, res) {
      res.render('me.ejs', {data: data});
    })

    app.post('/me', function (req, res) {
      data = req.body;
      res.render('me.ejs', {data: data});
    })

     app.listen(3000, function () {
   console.log('server work');
 });
